const startButton = document.getElementById('start-btn')
const reloadButton = document.getElementById('reload-btn')
const ansButton1 = document.getElementById('b1')
const ansButton2 = document.getElementById('b2')
const ansButton3 = document.getElementById('b3')
const ansButton4 = document.getElementById('b4')
const containerElement = document.getElementById('containerId')
const endElement = document.getElementById('endId')
const questionContainerElement = document.getElementById('question-container')
const endGameFailElement = document.getElementById('end-fail')
const endGameWinElement = document.getElementById('end-win')
const mess = document.getElementById('target')
const imgElement = document.getElementById('imgId')

const buttonContainer = document.getElementById('buttons-container')
const kolo1 = document.getElementById('k1')
const kolo2 = document.getElementById('k2')
const kolo3 = document.getElementById('k3')
const diagramContainer = document.getElementById('columnchart-material')

const questionElement = document.getElementById('question')
const answerButtonsElement = document.getElementById('answer-buttons')

let correctAnswerIndex,correctAns
let questionCounter = 1

startButton.addEventListener('click',startGame)

reloadButton.addEventListener('click',reloadGame)

function reloadGame() {
    location.reload()
}

function startGame(){
    imgElement.classList.add('hide')
    startButton.classList.add('hide')
    questionContainerElement.classList.remove('hide')

    buttonContainer.classList.remove('hide')

    console.log('dziala')
    mess.innerHTML = questionCounter
    setNextQuestion(questionCounter)
}

function setNextQuestion(){
    clickableOptions()
    clickableKola()

    $.ajax({
        url: 'http://127.0.0.1:8080/millionaire/question/'+ questionCounter,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log('succes',data);
            questionJ = data.question;
            window.answersJ = data.answers;
            window.correctAnswerJ = data.correctAnswer;

            showQuestion(questionJ,answersJ,correctAnswerJ)
        },
        error:  function() {
            console.log('błąd');
        }
    });
}

function showQuestion(questionJ,answersJ,correctAnswerJ){

    questionElement.innerHTML = questionJ

    ansButton1.innerHTML = answersJ[0]
    ansButton2.innerHTML = answersJ[1]
    ansButton3.innerHTML = answersJ[2]
    ansButton4.innerHTML = answersJ[3]

    correctAnswerIndex = correctAnswerJ
    correctAns = answersJ[correctAnswerIndex-1]

    kolo1.addEventListener('click',selectKolo1)
    kolo2.addEventListener('click',selectKolo2)
    kolo3.addEventListener('click',selectKolo3)

    ansButton1.addEventListener('click',selectAnswer)
    ansButton2.addEventListener('click',selectAnswer)
    ansButton3.addEventListener('click',selectAnswer)
    ansButton4.addEventListener('click',selectAnswer)

}

function selectKolo1(e){
    unclickableKola()
    console.log('klikniete kolo')
    const selectedKolo = e.target
    selectedKolo.classList.add('used')
    selectedKolo.classList.add('wrong')

    ansButton1.innerHTML = window.answersJ[0]
    ansButton2.innerHTML = window.answersJ[1]
    ansButton3.innerHTML = window.answersJ[2]
    ansButton4.innerHTML = window.answersJ[3]

    const ansArray = [ansButton1,ansButton2,ansButton3,ansButton4];

    correctAnswerIndex = window.correctAnswerJ
    correctAns = window.answersJ[correctAnswerIndex-1]

    if(questionCounter<6){
        for(let i=0;i<4;i++){
            let currentButton=window.answersJ[i]
            if(currentButton===correctAns){
                ansArray[i].classList.add('hint')
                break
            }
        }
    }else{
        let randomNumber = getRandom(0,4)
        ansArray[randomNumber].classList.add('hint')
    }

}

function selectKolo2(e){
    unclickableKola()
    console.log('klikniete kolo')
    const selectedKolo = e.target
    selectedKolo.classList.add('used')
    selectedKolo.classList.add('wrong')

    ansButton1.innerHTML = window.answersJ[0]
    ansButton2.innerHTML = window.answersJ[1]
    ansButton3.innerHTML = window.answersJ[2]
    ansButton4.innerHTML = window.answersJ[3]
    const ansArray = [ansButton1,ansButton2,ansButton3,ansButton4];

    correctAnswerIndex = window.correctAnswerJ
    correctAns = window.answersJ[correctAnswerIndex-1]

    let indexArray = [9,9]
    let i=0;

    while(indexArray.includes(9)){
        while(i<2){
            x = getRandom(0,4)
            if(x!==correctAnswerIndex-1 && !indexArray.includes(x)){
                indexArray[i] = x;
                i++;
            }
        }
    }

    ansArray[indexArray[0]].classList.add('hide')
    ansArray[indexArray[1]].classList.add('hide')
}

function selectKolo3(e) {
    unclickableKola()
    console.log('klikniete kolo')
    const selectedKolo = e.target
    selectedKolo.classList.add('used')
    selectedKolo.classList.add('wrong')

    ansButton1.innerHTML = window.answersJ[0]
    ansButton2.innerHTML = window.answersJ[1]
    ansButton3.innerHTML = window.answersJ[2]
    ansButton4.innerHTML = window.answersJ[3]

    correctAnswerIndex = window.correctAnswerJ
    correctAns = window.answersJ[correctAnswerIndex - 1]

    let percentage = [101,101,101,101];

    percentage[correctAnswerIndex-1] = getRandom(50,100)

    let percentageSum = percentage[correctAnswerIndex-1];
    let amountEmpty = 3;

    for(let i=0;i<4;i++) {
        if(i!==correctAnswerIndex-1) {
            if(amountEmpty===1){
                percentage[i]=100-percentageSum;
            } else {
                percentage[i] = getRandom(0, 99 - percentageSum)
                percentageSum += percentage[i];
                amountEmpty--;
            }
        }
    }

    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var options = {
            chart: {
                legend: 'none',
                format: 'percent'
            }
        };

        var data = google.visualization.arrayToDataTable([
            ['Głosy publiczności',' '],
            ['A', percentage[0]],
            ['B', percentage[1]],
            ['C', percentage[2]],
            ['D', percentage[3]]
        ]);

        var chart = new google.charts.Bar(diagramContainer);
        chart.draw(data, google.charts.Bar.convertOptions(options));

        diagramContainer.classList.remove('hide')
    }
}

function selectAnswer(e){
    unclickableOptions()
    const selectedButton = e.target
    if(selectedButton.innerHTML === correctAns){
        selectedButton.classList.remove('hint')
        selectedButton.classList.add('correct')

        if(questionCounter === 12){
            setTimeout(endGameWin,3000)
        }else{
            setTimeout(nextQuestion,3000,selectedButton)
        }

    }else{
        selectedButton.classList.remove('hint')
        selectedButton.classList.add('wrong')

        const buttonLen = answerButtonsElement.children.length
        for(let i=0; i<buttonLen; i++){
            if(answerButtonsElement.children[i].innerHTML === correctAns){       // pokazanie dobrej odpowiedzi
                answerButtonsElement.children[i].classList.remove('hint')
                answerButtonsElement.children[i].classList.add('correct')
            }
        }
        setTimeout(endGameFail,3000)
    }
}

// żeby iść do kolejego pytania
function nextQuestion(selectedButton){

    questionCounter++
    mess.innerHTML = questionCounter
    removeCorrect(selectedButton)   // usunąć zielony kolor
    setNextQuestion(questionCounter)
}

function removeCorrect(selectedB){
    selectedB.classList.remove('correct')
    ansButton1.classList.remove('hint')
    ansButton2.classList.remove('hint')
    ansButton3.classList.remove('hint')
    ansButton4.classList.remove('hint')
    ansButton1.classList.remove('hide')
    ansButton2.classList.remove('hide')
    ansButton3.classList.remove('hide')
    ansButton4.classList.remove('hide')
    diagramContainer.classList.add('hide')

}

// żeby nie móc wybrać drugi raz
function unclickableOptions(){

    ansButton1.classList.add('already-answered')
    ansButton2.classList.add('already-answered')
    ansButton3.classList.add('already-answered')
    ansButton4.classList.add('already-answered')

    unclickableKola()
}

// żeby dało się wybrać tylko jedno koło na raz
function unclickableKola(){
    kolo1.classList.add('already-answered')
    kolo2.classList.add('already-answered')
    kolo3.classList.add('already-answered')
}

// żeby móc wybierać z powrotem
function clickableOptions(){

    ansButton1.classList.remove('already-answered')
    ansButton2.classList.remove('already-answered')
    ansButton3.classList.remove('already-answered')
    ansButton4.classList.remove('already-answered')

    clickableKola()
}

// żeby nie użyte koła były dostępne z powrotem
function clickableKola(){
    kolo1.classList.remove('already-answered')
    kolo2.classList.remove('already-answered')
    kolo3.classList.remove('already-answered')
}

// po wygraniu gry
function endGameWin(){

    endElement.classList.remove('hide')
    questionContainerElement.classList.add('hide')
    containerElement.classList.add('hide')
    mess.classList.add('hide')
    diagramContainer.classList.add('hide')
    endGameWinElement.classList.remove('hide')

}

// po przegraniu gry
function endGameFail(){

    endElement.classList.remove('hide')
    questionContainerElement.classList.add('hide')
    containerElement.classList.add('hide')
    mess.classList.add('hide')
    diagramContainer.classList.add('hide')
    endGameFailElement.classList.remove('hide')

}

// losuje liczbę w zbiorze <0,4)
function getRandom(min,max){
    return Math.floor(Math.random() * (max - min) + min)
}